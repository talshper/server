const mongoose = require('mongoose');
const schema = mongoose.Schema;

const reviewSchema = new schema({
    reviewerId: { type: schema.Types.ObjectId, required: true },
    movieId: { type: schema.Types.ObjectId, required: true, ref: 'Movie' },
    content: { type: String },
    rating: { type: Number, required: true },
});

module.exports = mongoose.model('Review', reviewSchema);
