const User = require('../models/user');
const Review = require('../models/review');

const getAllUsers = async () => User.find({}, { password: 0 });

const getUserById = async (_id) => User.findById({ _id }, { password: 0 });

const getUserByUsername = async (username) => User.findOne({ username }, { password: 0 });

const validatePassword = async (username, password) => User.exists({ username, password });

const upsertUser = (_id, user) => User.findOneAndUpdate({ _id }, user, {
    new: true,
    upsert: true
});

const removeUserById = (_id) => Promise.all([User.findOneAndDelete({ _id }), Review.deleteMany({ reviewerId: _id })]);

const removeUsersByIds = async (_ids) => User.deleteMany().where('_id').in(_ids);

module.exports = {
    getAllUsers,
    getUserById,
    getUserByUsername,
    validatePassword,
    upsertUser,
    removeUserById,
    removeUsersByIds
};
