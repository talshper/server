const express = require('express');
const http = require('http');
const config = require('./config/config');
const bodyParser = require('body-parser');
const cors = require('cors');
const initRoutes = require('./routes/index');
const initDBConnection = require('./db/index');
const fetchExternalMovies = require('./services/external-movies-service');
const fetchExternalGenres = require('./services/external-genres-service');
const initWs = require('./services/client-count-service');

const app = express();
const server = http.createServer(app);

initWs(server);

app.use(cors());
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: false }));

app.use(function (req, res, next) {
    res.header('Access-Control-Allow-Origin', "*");
    res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");
    res.header('Access-Control-Allow-Credentials', true);
    res.header('Access-Control-Allow-Methods', 'GET, POST, OPTIONS, PUT, PATCH, DELETE');
    next();
});

initRoutes(app);
initDBConnection().then(() => {
    fetchExternalMovies();
    fetchExternalGenres();
}).catch(error => console.error(error));

server.listen(config.PORT, () => {
  console.log(`Movies server is running on localhost:${config.PORT}`);
});
