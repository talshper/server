const express = require('express');
const router = express.Router();
const Review = require('../db/models/review');
const reviewsController = require('../db/controller/reviews-controller');

router.get('/', async (request, response) => {
    const reviews = await reviewsController.getAllReviews();
    response.send(reviews);
});

router.get('/count', async (request, response) => {
    const reviews = await reviewsController.getAllReviews();
    response.json(reviews.length);
});

router.get('/:id', async (request, response) => {
    try {
        const review = await reviewsController.getReviewById(request.params.id);
        if (review) {
            response.send(review);
        } else {
            response.status(404).send(`Review ${request.params.id} not found.`);
        }
    } catch (e) {
        response.status(500).end();
    }
});

router.get('/movie-reviews/:id', async (request, response) => {
    try {
        const reviews = await reviewsController.getReviewsByMovieId(request.params.id);
        if (reviews) {
            response.send(reviews);
        } else {
            response.status(404).send(`Review of movie ${request.params.id} not found.`);
        }
    } catch (e) {
        response.status(500).end();
    }
});

router.get('/user/:id', async (request, response) => {
    try {
        const reviews = await reviewsController.getReviewsByUserId(request.params.id);
        reviews.map(review => {
            review.movie = review.movieId;
            delete review['movieId'];
            return review;
        })
        if (reviews) {
            response.send(reviews);
        } else {
            response.status(404).send(`Review of movie ${request.params.id} not found.`);
        }
    } catch (e) {
        response.status(500).end();
    }
});

router.put('/:id', async (request, response) => {
    try {
        const updatedReview = await reviewsController.upsertReview(request.body.review);
        if (updatedReview) {
            response.send(updatedReview);
        } else {
            response.status(404).send(`Review ${request.params.id} not found.`);
        }
    } catch (e) {
        response.status(500).end();
    }
});

router.post('/', async (request, response) => {
    const {reviewerId, movieId, content, rating} = request.body;
    try {
        const newReview = await reviewsController.upsertReview(new Review({reviewerId, movieId, content, rating}));
        response.send(newReview);
    } catch (err) {
        response.status(500).send(err);
    }
});

router.delete('/:id', async (request, response) => {
    try {
        const deletedReview = await reviewsController.removeReviewById(request.params.id);
        if (deletedReview) {
            response.status(200).send({messase: `Successfully deleted review ${deletedReview.id}`});
        } else {
            response.status(404).send(`Review ${request.params.id} not found.`)
        }
    } catch (e) {
        response.status(500).end();
    }
});

router.post('/delete', async (request, response) => {
    try {
        const deletedReviews = await reviewsController.removeReviewsByIds(request.body);
        if (deletedReviews) {
            response.send(`Successfully deleted reviews`);
        } else {
            response.status(404).send(`Reviews not found.`)
        }
    } catch (e) {
        response.status(500).end();
    }
});

module.exports = router;
