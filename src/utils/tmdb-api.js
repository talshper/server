const axios = require('axios');
const tmdbConfig = require('../config/config').TMDB;

const getAll = (path) => axios.get(`${tmdbConfig.API_URL}/${path}?api_key=${tmdbConfig.API_KEY}`);

const getEntityById = (path, id) =>
    axios.get(`${tmdbConfig.API_URL}/${path}/${id}?api_key=${tmdbConfig.API_KEY}`);

module.exports = {
    getAll,
    getEntityById
};
