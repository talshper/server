const express = require('express');
const router = express.Router();
const Movie = require('../db/models/movie');
const moviesController = require('../db/controller/movies-controller');
const reviewsController = require('../db/controller/reviews-controller');

router.get('/', async (request, response) => {
    try {
        const movies = await moviesController.getAllMovies();
        const reviews = await reviewsController.getAllReviews();

        for (let movieIndex in movies) {
            let currMovieReviews = [];
            for (let reviewIndex in reviews) {
                if (reviews[reviewIndex].movieId.equals(movies[movieIndex]._id)) {
                    currMovieReviews.push(reviews[reviewIndex]);
                }
            }

            if (currMovieReviews) {
                movies[movieIndex].rating = moviesController.calculateAverageRating(currMovieReviews);
            }
        }

        response.send(movies);
    } catch (e) {
        response.status(500).end();
    }
});

router.get('/count', async (request, response) => {
    const movies = await moviesController.getAllMovies();
    response.json(movies.length);
});

router.get('/genreStatistics', async (request, response) => {
    const statistics = await moviesController.getAmountGroupByGeneres();

    if (statistics) {
        response.status(200).send(statistics);
    } else {
        response.status(404).send(`Movies not found.`)
    }
})

router.get('/:id', async (request, response) => {
    try {
        const movie = await moviesController.getMovieById(request.params.id);
        const rating = moviesController.calculateMovieRating;

        if (movie && rating) {
            movie.rating = rating;
            response.send(movie);
        } else {
            response.status(404).end();
        }
    } catch (e) {
        response.status(500).end();
    }


    /*    /!*TODO: RUN ON LOCAL DB*!/
        const movie = await moviesController.getMovieById(request.params.id);
        const rating = await moviesController.calculateMovieRating(request.params.id);

        if (movie && rating) {
            movieAvgRating = rating.results.filter((val) => val.value.movieId.equals(movie._id));
            movie.rating = (movieAvgRating.length > 0) ? (movieAvgRating[0].value.avg) : 0;
            response.send(movie);
        } else {
            response.status(404).send(`Movie ${request.params.id} not found.`);
        }*/

});

router.put('/', async (request, response) => {
    try {
        const {_id, title, description, imageUrl, rating} = request.body;
        const newMovie = await moviesController.upsertMovie({_id, title, description, imageUrl, rating});
        response.send(newMovie);
    } catch (e) {
        response.status(500).end();
    }
});

router.post('/', async (request, response) => {
    try {
        const {externalApiId, title, description, imageUrl, rating, genresApiIds} = request.body;
        const newMovie = await moviesController.upsertMovie(new Movie({
            externalApiId,
            title,
            description,
            imageUrl,
            rating,
            genresApiIds
        }));
        response.send(newMovie);
    } catch (e) {
        response.status(500).end();
    }
});

router.delete('/:id', async (request, response) => {
    try {
        const deletedMovie = await moviesController.removeMovie(request.params.id);
        if (deletedMovie[0]) {
            response.send(`Successfully deleted movie ${deletedMovie[0]._id}`);
        } else {
            response.status(404).send(`Movie ${request.params.id} not found.`)
        }
    } catch (e) {
        response.status(500).end();
    }

});

router.post('/delete', async (request, response) => {
    try {
        const deletedMovies = await moviesController.removeMoviesByIds(request.body);
        const deletedReviews = await reviewsController.removeReviewsByMovieIds(request.body);

        if (deletedMovies && deletedReviews) {
            response.send(`Successfully deleted movies and reviews`);
        } else {
            response.status(404).send(`Movies not found.`)
        }
    } catch (e) {
        response.status(500).end();
    }
});

module.exports = router;
