const mongoose = require('mongoose');
const schema = mongoose.Schema;

const genreSchema = new schema({
    externalAPIid: { type: String, required: true },
    name: { type: String, required: true }
});

module.exports = mongoose.model('Genre', genreSchema);
