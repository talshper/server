const mongoose = require('mongoose');
const schema = mongoose.Schema;

const movieSchema = new schema({
    externalAPIid: { type: String },
    title: { type: String, required: true },
    description: { type: String },
    imageUrl: { type: String },
    rating: { type: Number, default: 0 },
    reviews: { type: [schema.Types.ObjectId], default: [] },
    genresApiIds: { type: [String], default: [] },
});

module.exports = mongoose.model('Movie', movieSchema);
