const Movie = require('../models/movie');
const Review = require('../models/review');
const Genre = require('../models/genre');

const getAllMovies = async () => Movie.find();

const getMovieById = async (_id) => Movie.findById(_id);

const upsertMovie = (movie) => Movie.findOneAndUpdate({_id: movie._id}, { $set: movie }, {
    new: true,
    upsert: true
});
const removeMovie = (_id) => Promise.all([Movie.findOneAndDelete({_id}), Review.deleteMany({movieId: _id})]);

const bulkUpsert = (movies) => Movie.bulkWrite(movies.map(movie => ({
    updateOne: {
        filter: {externalAPIid: movie.externalAPIid},
        update: movie,
        upsert: true
    }
})));

const calculateMovieRating = 4.5;

function calculateAverageRating(movieReviews) {
    let ratingAverage = 0;

    movieReviews.forEach(function (entry) {
        ratingAverage += entry.rating;
    });

    ratingAverage /= movieReviews.length;

    return roundRating(ratingAverage);
}

const roundRating = (rating) => Math.round(rating * 10) / 10;

/*TODO: RUN ON LOCAL DB*/
// const o ={};
//
// o.map = function () {
//     emit(this.movieId , { ratings : this.rating , count : 1});
// };
//
// o.reduce = function (key, values) {
//     const n = {ratings : 0, count : 0, movieId: key};
//
//     for (let i=0; i<values.length; i++ ){
//         n.ratings += values[i].ratings;
//         n.count += values[i].count;
//     }
//
//     return n;
// };
//
// o.finalize = function (key, res) {
//     res.avg = Math.round( (res.ratings / res.count) * 10) / 10;
//     return res;
// }
//
// const calculateMovieRating = async (_id) =>
//     Review.mapReduce(o);

const removeMoviesByIds = async (_ids) => Movie.deleteMany().where('_id').in(_ids);

const getAmountGroupByGeneres = async () => {
    return Movie.aggregate([{
        $unwind: '$genresApiIds'
    }, {
        $lookup: {
            from: Genre.collection.name,
            localField: 'genresApiIds',
            foreignField: 'externalAPIid',
            as: 'genre'
        },

    }, {
        $group: {
            _id: '$genre.name',
            amount: {$sum: 1}
        }
    }, {
        $unwind: '$_id'
    }, {
        $project: {_id: 0, 'title': '$_id', 'amount': 1}
    }, {
        $sort: { title: 1 }
    }]);
}

module.exports = {
    getAllMovies,
    getMovieById,
    upsertMovie,
    removeMovie,
    bulkUpsert,
    calculateMovieRating,
    calculateAverageRating,
    removeMoviesByIds,
    getAmountGroupByGeneres
};
