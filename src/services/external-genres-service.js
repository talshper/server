const tmdbApi = require('../utils/tmdb-api');
const genresController = require('../db/controller/genres-controller');
const { yellowBright, green } = require('chalk');

const fetchExternalGenres = async () => {
    try {
        const movieGenres = await getMoviesGenres();
        console.log(green(`Got ${movieGenres.length} genres from API. Inserting to local DB`));

        const result =  await genresController.bulkUpsert(movieGenres.map(formatGenre));
        console.log(
            yellowBright([
                `Modified Genres: ${result.modifiedCount}`,
                `\nExisting Genres: ${result.matchedCount}`,
                `\nUpsertedCount: Genres: ${result.upsertedCount}`
            ])
        );

        console.log(green('Finished inserting genres to DB'));
    } catch (err) {
        console.error('External Genres Service Error: \n' + err);
    }
};

const getMoviesGenres = async () => (await tmdbApi.getAll('genre/movie/list')).data.genres;

const formatGenre = (genre) => ({ externalAPIid: genre.id, name: genre.name });

module.exports = fetchExternalGenres;
