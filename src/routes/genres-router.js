const express = require('express');
const router = express.Router();
const genresController = require('../db/controller/genres-controller');

router.get('/:_ids', async (request, response) => {
    try {
        const genres = await genresController.getGenresByIds(request.params._ids.split(','));
        if (genres) {
            response.send(genres);
        } else {
            response.status(404).send(`Genre ${request.params.id} not found.`);
        }
    } catch (e) {
        response.status(500).end();
    }
});

router.get('/', async (request, response) => {
    const genres = await genresController.getAllGenres();
    if (genres) {
        response.send(genres);
    } else {
        response.status(404).send(`Genres not found.`);
    }
});

module.exports = router;
