const io = require('socket.io');

let connectedCount = 0;
let ws;

const initWs = (http) => {
    ws = io(http);
    ws.on('connection', onConnect);
};

const onConnect = (socket) => {
    connectedCount++;
    socket.on('disconnect', () => {
        connectedCount--;
        ws.sockets.emit('newCount', { connectedCount });
        console.log('client disconnected - ', connectedCount);
    });
    ws.sockets.emit('newCount', { connectedCount });
    console.log('client connected - ', connectedCount);
}

module.exports = initWs;
