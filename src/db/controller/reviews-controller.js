const Review = require('../models/review');

const getAllReviews = async () => Review.find();

const getReviewById = async (_id) => Review.findById(_id);

const getReviewsByUserId = async (_id) =>
    Review.find({'reviewerId': _id}).populate('movieId', ['title', 'imageUrl', "_id"]).lean();

const getReviewsByMovieId = async (_id) => Review.find({ 'movieId': _id});

const getReviewsOfMovies = async (ids) => Review.find().where('movieId').in(ids);

const upsertReview = async (review) => {
    return Review.findOneAndUpdate({_id: review._id}, review, {
        new: true,
        upsert: true
    });
}

const removeReviewById = async (_id) => Review.findOneAndDelete({_id});

const removeReviewsByIds = async (_ids) => Review.deleteMany().where('_id').in(_ids);

const removeReviewsByMovieIds = async (_ids) => Review.deleteMany().where('movieId').in(_ids);

const removeReviewsByUserIds = async (_ids) => Review.deleteMany().where('reviewerId').in(_ids);

module.exports = {
    getAllReviews,
    getReviewById,
    getReviewsByUserId,
    getReviewsByMovieId,
    upsertReview,
    removeReviewById,
    getReviewsOfMovies,
    removeReviewsByIds,
    removeReviewsByMovieIds,
    removeReviewsByUserIds
}
