const Genre = require('../models/genre');

const getGenresByIds = async (_ids) => Genre.find().where('externalAPIid').in(_ids);

const getAllGenres = async () => Genre.find();

const bulkUpsert = (genres) => {
    return Genre.bulkWrite(genres.map(genre => ({
        updateOne: {
            filter: {externalAPIid: genre.externalAPIid},
            update: genre,
            upsert: true
        }
    })));
};

module.exports = {
    bulkUpsert,
    getAllGenres,
    getGenresByIds
};
