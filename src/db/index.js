const mongoose = require('mongoose');
const config = require('../config/config');

function initDBConnection() {
    return new Promise((resolve, reject) => {
        mongoose.connect(config.DB.URL, {
            useNewUrlParser: true,
            useUnifiedTopology: true,
            useFindAndModify: false
        }).catch((error) => {
            reject(error);
        });

        mongoose.connection.on('error',  () => {
            console.error.bind(console, 'connection error:');
        });

        mongoose.connection.once('open', () => {
            console.log("Mongodb is connected");
            resolve();
        });
    });
}

module.exports = initDBConnection;