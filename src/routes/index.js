const reviewsRouter = require('./reviews-router');
const usersRouter = require('./users-router');
const moviesRouter = require('./movies-router');
const genresRouter = require('./genres-router');

function initRoutes(app) {
    app.get('/', (req, res) => {
        res.send('Welcome to the movies server!')
    });

    app.use('/reviews', reviewsRouter);
    app.use('/users', usersRouter);
    app.use('/movies', moviesRouter);
    app.use('/genres', genresRouter);

}

module.exports = initRoutes;
