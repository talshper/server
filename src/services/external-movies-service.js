const moviesController = require('../db/controller/movies-controller');
const tmdbAPI = require('../utils/tmdb-api');
const { green, yellowBright } = require('chalk');

const fetchExternalMovies = async () => {
    try {
        const popularMovies = await getPopularMovies();
        const imageBaseURL = await getTMDBimageURL();

        const extendedMovies = popularMovies.map((movie) => getAdditionalMovieData(movie, imageBaseURL));

        Promise.all(extendedMovies).then(upsertMovies);
    } catch (err) {
        console.error('External Movies Service Error: \n' + err)
    }
};

const getPopularMovies = async () => (await tmdbAPI.getAll('movie/popular')).data.results;

const upsertMovies = async (moviesToUpsert) => {
    console.log(green(`Got ${moviesToUpsert.length} movies from API. Inserting to local DB`));

    const result = await moviesController.bulkUpsert(moviesToUpsert);
    console.log(
        yellowBright([
            `Modified Movies: ${result.modifiedCount}`,
            `\nExisting Movies: ${result.matchedCount}`,
            `\nUpserted Movies: ${result.upsertedCount}`
        ])
    );

    console.log(green('Finished inserting movies to DB'));
};

const getTMDBimageURL = async () => (await tmdbAPI.getAll('configuration')).data.images.base_url;

const getAdditionalMovieData = async (movie, tmdbImageURL) => {
    const result = await tmdbAPI.getEntityById('movie', movie.id);
    if (result.status === 200) {
        return createMovieObjectFromTMDB(result.data, tmdbImageURL);
    } else {
        console.warn(`Failed fetching additional data for movie ${movie.id} - ${movie.title}`);
    }
};

const createMovieObjectFromTMDB = (movie, imagePathPrefix) => {
    const imageSize = 'w500';

    return {
        externalAPIid: movie.id,
        title: movie.title,
        description: movie.overview,
        imageUrl: `${imagePathPrefix}${imageSize}${movie.poster_path}`,
        genresApiIds: movie.genres.map((genre) => genre.id.toString())
    };
};

module.exports = fetchExternalMovies;
