const express = require('express');
const router = express.Router();
const User = require('../db/models/user');
const usersController = require('../db/controller/users-controller');
const reviewsController = require('../db/controller/reviews-controller');

router.get('/', async (request, response) => {
    const users = await usersController.getAllUsers();
    response.send(users);
});

router.get('/count', async (request, response) => {
    const users = await usersController.getAllUsers();
    response.json(users.length);
});

router.get('/:id', async (request, response) => {
    try {
        const user = await usersController.getUserById(request.params.id);
        if (user) {
            delete user.password;
            response.send(user);
        } else {
            response.status(404).send(`User ${request.params.id} not found.`);
        }
    } catch (e) {
        response.status(500).end();
    }
});

router.get('/:username', async (request, response) => {
    try {
        const user = await usersController.getUserByUsername(request.params.username);
        if (user) {
            delete user.password;
            response.send(user);
        } else {
            response.status(404).send(`User ${request.params.username} not found.`);
        }
    } catch (e) {
        response.status(500).end();
    }
});

router.put('/:id', async (request, response) => {
    try {
        const updatedUser = await usersController.upsertUser(request.params.id, request.body);
        if (updatedUser) {
            response.send(updatedUser);
        } else {
            response.status(404).send(`User ${request.params.id} not found.`);
        }
    } catch (e) {
        response.status(500).end();
    }
});

router.post('/login', async (request, response) => {
    try {
        const {username, password} = request.body;
        const existingUser = await usersController.getUserByUsername(username);

        if (existingUser) {
            const isPasswordValid = await usersController.validatePassword(username, password);

            if (isPasswordValid) {
                response.send(existingUser);
            } else {
                response.status(401).send(`User/Password are incorrect`);
            }
        }
    } catch (e) {
        response.status(500).end();
    }
});

router.post('/', async (request, response) => {
    try {
        const {username, password, firstName, lastName, isAdmin = false} = request.body;
        const existingUser = await usersController.getUserByUsername(username);
        if (existingUser) {
            response.status(409).send(`User ${username} already exists.`);
        } else {
            const user = new User({
                username,
                password,
                firstName,
                lastName,
                isAdmin
            });

            const newUser = await usersController.upsertUser(user._id, user);
            response.send(newUser);
        }
    } catch (e) {
        response.status(500).end();
    }
});

router.post('/signIn', async (request, response) => {
    try {
        const {username, password} = request.body;
        const isLoginValid = await usersController.validatePassword(username, password);
        if (isLoginValid) {
            response.send(await usersController.getUserByUsername(username));
        } else {
            response.status(401).send(` המשתמש ${username} לא קיים או שהסיסמה לא נכונה `);
        }
    } catch (e) {
        response.status(500).end();
    }
});

router.delete('/:id', async (request, response) => {
    try {
        const deletedUser = await usersController.removeUserById(request.params.id);
        if (deletedUser[0]) {
            response.send(`Successfully deleted user ${deletedUser[0]._id} - ${deletedUser[0].username}`);
        } else {
            response.status(404).send(`User ${request.params.id} not found.`)
        }
    } catch (e) {
        response.status(500).end();
    }
});

router.post('/delete', async (request, response) => {
    try {
        const deletedUsers = await usersController.removeUsersByIds(request.body);
        const deletedReviews = await reviewsController.removeReviewsByUserIds(request.body);

        if (deletedUsers && deletedReviews) {
            response.send(`Successfully deleted users and reviews`);
        } else {
            response.status(404).send(`Users not found.`)
        }
    } catch (e) {
        response.status(500).end();
    }
});

module.exports = router;
